#!/bin/bash
docker run \
        -v ${PWD}:/home/icecube/powershovel \
        -it \
        -p 8501:8501 \
        ehrhorn/cubedev:0.1 \
        streamlit run /home/icecube/powershovel/powershovel.py