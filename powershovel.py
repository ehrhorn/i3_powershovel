import math
import os
from pathlib import Path
import pickle

import numpy as np
import pandas as pd
import plotly.express as px
import streamlit as st

# from i3_reader import fetch_data
from i3_reader import i3_to_dataframe

# from plots import plotly_event


def _max_width_():
    max_width_str = f"max-width: 2000px;"
    st.markdown(
        f"""
    <style>
    .reportview-container .main .block-container{{
        {max_width_str}
    }}
    </style>
    """,
        unsafe_allow_html=True,
    )


_max_width_()


def gaussian(x, mean, std, height):
    return (
        height
        / (std * math.sqrt(2 * math.pi))
        * math.exp(-1 / 2 * ((x - mean) / std) ** 2)
    )


@st.cache
def save_events(files, gcd_file, keys):
    df = i3_to_dataframe(files, gcd_file, keys)
    with open(TEMP_DIR.joinpath("events.pkl"), "wb") as f:
        pickle.dump(df, f)


@st.cache
def load_events(bins=24):
    with open(TEMP_DIR.joinpath("events.pkl"), "rb") as f:
        df = pickle.load(f)
    df[1]["binned"] = pd.cut(df[1]["true_primary_energy"], bins)
    return df


@st.cache
def create_event_df(df, event):
    trimmed_df = df[df["event_id"] == event]
    dom_x = []
    dom_y = []
    dom_z = []
    dom_time = []
    dom_charge = []
    timestamp = []
    group = []
    for j, (index, row) in enumerate(trimmed_df.iterrows()):
        for i in range(0, 200):
            dom_x.append(row["dom_x"])
            dom_y.append(row["dom_y"])
            dom_z.append(row["dom_z"])
            dom_time.append(row["dom_time"])
            dom_charge.append(gaussian(i, row["dom_time"] / 100, 1, 1))
            timestamp.append(i)
            group.append(j)
    anim_df = pd.DataFrame(
        data={
            "dom_x": dom_x,
            "dom_y": dom_y,
            "dom_z": dom_z,
            "dom_time": dom_time,
            "dom_charge": dom_charge,
            "timestamp": timestamp,
            "group": group,
        }
    )
    return anim_df


sequential_keys = [
    "event_id",
    "dom_x",
    "dom_y",
    "dom_z",
    "dom_time",
    "dom_charge",
    "SplitInIcePulses",
    "SRTInIcePulses",
]
scalar_keys = [
    "event_id",
    "true_primary_energy",
    "true_primary_time",
    "true_primary_direction_x",
    "true_primary_direction_y",
    "true_primary_direction_z",
    "true_primary_position_x",
    "true_primary_position_y",
    "true_primary_position_z",
]
keys = (sequential_keys, scalar_keys)
ROOT_DIR = Path(__file__).parent.absolute()
TEMP_DIR = ROOT_DIR.joinpath("temp")
if not TEMP_DIR.is_dir():
    TEMP_DIR.mkdir()

st.sidebar.markdown("# Powershovel")

uploaded_gcd = st.sidebar.file_uploader("Upload gcd file")
if uploaded_gcd is not None:
    file_path = TEMP_DIR.joinpath("gcd.i3.gz")
    with open(file_path, "wb") as f:
        f.write(uploaded_gcd.read())
    gcd_file = TEMP_DIR.joinpath("gcd.i3.gz")

if uploaded_gcd is not None:
    uploaded_file = st.sidebar.file_uploader("Upload i3 file(s)")
    if uploaded_file is not None:
        files = []
        if not isinstance(uploaded_file, list):
            uploaded_file = [uploaded_file]
        for i, file in enumerate(uploaded_file):
            file_name = str(i) + ".i3.zst"
            file_path = TEMP_DIR.joinpath(file_name)
            with open(file_path, "wb") as f:
                f.write(file.read())
            files.append(str(file_path))
        with st.spinner("Reading i3 file"):
            save_events(files, gcd_file, keys)

    page = st.sidebar.radio("Page", ("Event viewer", "Distributions"))

    if page == "Event viewer":
        animate = st.sidebar.radio("Animate?", ("Yes", "No"), index=1)
        if not TEMP_DIR.joinpath("events.pkl").is_file():
            st.write("Upload file!")
        else:
            sequential_df, scalar_df = load_events()
            bins = scalar_df["binned"].unique().sort_values()
            selected_bin = st.sidebar.slider(
                "Bin", min_value=0, max_value=len(bins) - 1
            )
            event_ids = scalar_df[scalar_df["binned"] == bins[selected_bin]]["event_id"]
            event_id = np.random.choice(event_ids)
            st.write(
                "Showing event id {} from energy bin {}".format(
                    event_id, bins[selected_bin]
                )
            )
            if animate == "Yes":
                anim_df = create_event_df(sequential_df, event_id)
                fig = px.scatter_3d(
                    anim_df,
                    x="dom_x",
                    y="dom_y",
                    z="dom_z",
                    animation_frame="timestamp",
                    animation_group="group",
                    size="dom_charge",
                    color="dom_time",
                    range_x=[-800, 800],
                    range_y=[-800, 800],
                    range_z=[-800, 800],
                    height=800,
                    color_continuous_scale="Bluered",
                )
                fig.layout.updatemenus[0].buttons[0].args[1]["frame"]["duration"] = 20
                fig.layout.updatemenus[0].buttons[0].args[1]["transition"][
                    "duration"
                ] = 20
            else:
                plot_df = sequential_df[sequential_df["event_id"] == event_id]
                fig = px.scatter_3d(
                    plot_df,
                    x="dom_x",
                    y="dom_y",
                    z="dom_z",
                    size="dom_charge",
                    color="dom_time",
                    range_x=[-800, 800],
                    range_y=[-800, 800],
                    range_z=[-800, 800],
                    height=800,
                    color_continuous_scale="Bluered",
                )
            st.plotly_chart(fig, use_container_width=True)
    elif page == "Distributions":
        if not TEMP_DIR.joinpath("events.pkl").is_file():
            st.write("Upload file!")
        else:
            sequential_df, scalar_df = load_events()
            variable_list = sequential_keys + scalar_keys
            if "event_id" in variable_list:
                variable_list.remove("event_id")
            if "SplitInIcePulses" in variable_list:
                variable_list.remove("SplitInIcePulses")
            if "SRTInIcePulses" in variable_list:
                variable_list.remove("SRTInIcePulses")
            if "event_id" in variable_list:
                variable_list.remove("event_id")
            variable = st.sidebar.selectbox("Variable", variable_list)
            plot_df = sequential_df if variable in sequential_keys else scalar_df
            fig = px.histogram(plot_df, x=variable, histnorm="probability density")
            st.plotly_chart(fig)
