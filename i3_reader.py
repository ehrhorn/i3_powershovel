from pathlib import Path

import numpy as np
import pandas as pd

from I3Tray import I3Tray

from icecube import icetray
from icecube import dataclasses

from icecube.common_variables import time_characteristics


def fetch_data(frame, inputs):
    sequential = inputs[0]
    scalar = inputs[1]

    event_id = frame["I3EventHeader"].event_id

    uncleaned_pulses = frame["SplitInIcePulses"].apply(frame)
    cleaned_pulses = frame["SRTInIcePulses"].apply(frame)

    dom_geom = frame["I3Geometry"].omgeo

    temp = np.empty((0, 8))

    for entry in uncleaned_pulses.items():
        cleaned_time_list = []
        this_om_key = entry[0]
        if this_om_key in cleaned_pulses.keys():
            cleaned_temp = cleaned_pulses[this_om_key]
            for cleaned_entry in cleaned_temp:
                cleaned_time_list.append(cleaned_entry.time)
        this_om_geom = dom_geom[this_om_key]
        this_om_position = this_om_geom.position
        for pulse in entry[1]:
            uncleaned_mask = 1
            if pulse.time in cleaned_time_list:
                cleaned_mask = 1
            else:
                cleaned_mask = 0
            pulses = np.array(
                [
                    this_om_position.x,
                    this_om_position.y,
                    this_om_position.z,
                    pulse.time,
                    pulse.charge,
                    uncleaned_mask,
                    cleaned_mask,
                    event_id,
                ],
                ndmin=2,
            )
            temp = np.append(temp, pulses, axis=0)
    temp = temp[temp[:, 3].argsort()]
    for row in range(temp.shape[0]):
        sequential["event_id"].append(temp[row, 7].astype(np.uint16))
        sequential["dom_x"].append(temp[row, 0].astype(np.single))
        sequential["dom_y"].append(temp[row, 1].astype(np.single))
        sequential["dom_z"].append(temp[row, 2].astype(np.single))
        sequential["dom_time"].append(temp[row, 3].astype(np.uint16))
        sequential["dom_charge"].append(temp[row, 4].astype(np.single))
        sequential["SplitInIcePulses"].append(temp[row, 5].astype(np.bool_))
        sequential["SRTInIcePulses"].append(temp[row, 6].astype(np.bool_))

    try:
        scalar["event_id"].append(event_id)
        true_primary = dataclasses.get_most_energetic_primary(frame["I3MCTree"])
        true_muon = dataclasses.get_most_energetic_muon(frame["I3MCTree"])
        if true_muon is None:
            scalar["true_primary_energy"].append(np.log10(true_primary.energy))
        else:
            scalar["true_primary_energy"].append(np.log10(true_muon.energy))

        true_primary_direction = true_primary.dir
        true_primary_entry_position = true_primary.pos
        scalar["true_primary_direction_x"].append(true_primary_direction.x)
        scalar["true_primary_direction_y"].append(true_primary_direction.y)
        scalar["true_primary_direction_z"].append(true_primary_direction.z)
        scalar["true_primary_time"].append(true_primary.time)
        scalar["true_primary_position_x"].append(true_primary_entry_position.x)
        scalar["true_primary_position_y"].append(true_primary_entry_position.y)
        scalar["true_primary_position_z"].append(true_primary_entry_position.z)
    except Exception:
        scalar["event_id"].append(None)
        scalar["true_primary_energy"].append(None)
        scalar["true_primary_direction_x"].append(None)
        scalar["true_primary_direction_y"].append(None)
        scalar["true_primary_direction_z"].append(None)
        scalar["true_primary_time"].append(None)
        scalar["true_primary_position_x"].append(None)
        scalar["true_primary_position_y"].append(None)
        scalar["true_primary_position_z"].append(None)


def i3_to_dataframe(i3_files, gcd_file, keys):
    i3_files = [str(file) for file in i3_files]
    sequential = {key: [] for key in keys[0]}
    scalar = {key: [] for key in keys[1]}
    tray = I3Tray()
    tray.AddModule("I3Reader", "reader", FilenameList=[str(gcd_file)] + i3_files)
    tray.Add(fetch_data, "fetch_data", inputs=(sequential, scalar))
    tray.Execute()
    tray.Finish()
    sequential_df = pd.DataFrame(data=sequential)
    scalar_df = pd.DataFrame(data=scalar)
    return sequential_df, scalar_df
