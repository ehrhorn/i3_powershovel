# Powershovel: IceCube event/file viewer

Powershovel reads i3 files, and shows events (animated or not) and variable
distributions.
It _should_ be able to take several i3 files in one go, but that'll be slow.

Powershovel runs on a Docker image, which contains IceTray etc.

The `pyproject.toml` file exists so you can install a virtual environment using
Poetry, which contains `flake8` and `black`.

*HERE BE BUGS!!! PLEASE READ THE KNOWN BUGS SECTION BELOW*

## Installation

Clone this repo, ensure you have Docker installed on your machine, run
`docker_run_streamlit.sh`.
The Docker image should be pulled automatically from Docker Hub.
Note that the image is quite large, as it was build on top of a TensorFlow image;
that should be changed in the future.

Then, upload the gcd file for L5 OscNext, and then an i3 file from L5 OscNext
(not tested on other files, thus not guaranteed to work).

Let me know of any and all bugs and feature suggestions.

## Known bugs

1. For some reason, reloading Streamlit causes an
`ImportError: cannot import name 'converters'` exception in the import
`from icecube.common_variables import time_characteristics` in `i3_reader.py`.
This means that if you change code, you'll need to restart your Streamlit server
by hitting `ctrl-c` and running `docker_run_streamlit.sh` again.
This is a showstopper, and must be fixed ASAP.

2. For now, Powershovel only works on L5 OscNext neutrino i3 files.

## To do

1. Pretty up everything

2. Ensure that all types of i3 files work

3. Only pulses are shown; all DOMs should be shown on the event viewer

4. True directions and vertices

5. Reconstruction directions and vertices
